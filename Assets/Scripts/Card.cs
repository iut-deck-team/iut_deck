﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Card : MonoBehaviour
{
    public bool isPosed = false;
    public bool isSelected = false;
    public float speed = 1.0f;
    private Board board;
    private Choice choice;

    private void Start()
    {
        choice = GameObject.Find("Choice").GetComponent<Choice>();
    }


    void OnMouseDown()
    {
        if (!isSelected && !choice.isActive)
        {
            if(this.CompareTag("Serviteur"))
            {
                isSelected = true;
                choice.addCard(this);
                Animator animator = this.GetComponent<Animator>();
                animator.speed = 1.0f;
                if (animator != null)
                {
                    bool IsClicked = animator.GetBool("isClicked");
                    animator.SetBool("isClicked", true);
                }
            }
            Board board = GameObject.Find("Board").GetComponent<Board>();
            //board.addCard(this);
        }
    }

    void OnMouseEnter()
    {
        if(!isSelected && !choice.isActive)
        {
            Animator animator = this.GetComponent<Animator>();
            animator.speed = 2.0f;
            if (animator != null)
            {
                bool isHover = animator.GetBool("isHover");
                animator.SetBool("isHover", true);
            }
        }
        
    }

    void OnMouseExit()
    {
        if (!isSelected && !choice.isActive)
        {
            Animator animator = this.GetComponent<Animator>();
            if (animator != null)
            {
                bool isHover = animator.GetBool("isHover");
                animator.SetBool("isHover", false);
            }
        }
    }
}