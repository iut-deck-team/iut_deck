﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stateButton : MonoBehaviour
{
    Choice choice;
    Board board;

    private void Start()
    {
        choice = GameObject.Find("Choice").GetComponent<Choice>();
        board = GameObject.Find("Board").GetComponent<Board>();
    }
    void setAttaque()
    {
        choice.desactive();
        Animator animator = choice.actualCard.GetComponent<Animator>();
        if (animator != null)
        {
            bool isHover = animator.GetBool("poseCard");
            animator.SetBool("poseCard", true);
        }
        choice.actualCard.transform.SetParent(board.transform);
        choice.actualCard.transform.position = new Vector3(choice.actualCard.transform.position.x, choice.actualCard.transform.position.y, 0);
        choice.isActive = false;
    }

    void setDefence()
    {
        choice.desactive();
        Animator animator = choice.actualCard.GetComponent<Animator>();
        if (animator != null)
        {
            bool isHover = animator.GetBool("poseCard");
            animator.SetBool("poseCard", true);
        }
        Quaternion target = Quaternion.Euler(0, 0, -90);
        choice.actualCard.transform.localRotation = Quaternion.Slerp(choice.actualCard.transform.localRotation, target, 1);
        choice.actualCard.transform.position = new Vector3(choice.actualCard.transform.position.x, choice.actualCard.transform.position.y, 0);
        choice.actualCard.transform.SetParent(board.transform);
        choice.isActive = false;
    }

    public void OnMouseDown()
    {
        Debug.Log("hello");
        if(this.CompareTag("Attaque"))
        {
            setAttaque();
        }
        else
        {
            setDefence();
        }
    }
}
