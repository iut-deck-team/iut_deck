﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Choice : MonoBehaviour
{
    public bool isActive = false;
    bool transition = false;
    public Card actualCard;

    private void Start()
    {
        Vector3 v4 = new Vector3(this.transform.position.x, this.transform.position.y + 6000, -20);
        transform.position = v4;
    }

    public void active()
    {
        Vector3 v4 = new Vector3(this.transform.position.x, this.transform.position.y - 6000, -20);
        transform.position = v4;
    }

    public void desactive()
    {
        Vector3 v4 = new Vector3(this.transform.position.x, this.transform.position.y + 6000, -20);
        transform.position = v4;
    }

    public void addCard(Card card)
    {
        active();
        actualCard = card;
        card.transform.SetParent(this.transform);
        transition = true;
    }

    private void Update()
    {
        if(transition)
        {
            isActive = true;
            Vector3 v3 = new Vector3(1000, 0, -20);
            Vector3 v4 = new Vector3(actualCard.transform.localScale.x * 3, actualCard.transform.localScale.y * 3, 1);
            actualCard.transform.position = Vector3.MoveTowards(actualCard.transform.position, v3, 250);
            actualCard.transform.localScale = Vector3.MoveTowards(actualCard.transform.localScale, v4, 1);
            if (actualCard.transform.position == v3)
            {
                transition = false;
            }
        }
    }
}
