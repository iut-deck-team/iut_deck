﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public List<Card> hand;
    void Start()
    {
        hand = new List<Card>();
    }

    void addCard(Card card)
    {
        if(hand.Count >= 7)
        {
            return;
        }
        else
        {
            hand.Add(card);
            card.transform.SetParent(this.transform);
        }
    }

}
