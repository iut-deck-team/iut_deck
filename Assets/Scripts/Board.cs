﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public int nbCard;
    public List<Card> board = new List<Card>();

    public void addCard(Card card)
    {
        if(board.Count >= nbCard)
        {

            return;
        }
        else
        {
            card.transform.localRotation = Quaternion.Euler(0, 0, 90);
            card.isPosed = true;
            board.Add(card);
            card.transform.SetParent(this.transform);
        }
    }
}
